import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Home from "../components/home"
import { Provider } from 'react-redux'
import store from "../components/store"

// const store = configureStore()

const IndexPage = () => (
  <Provider store={store}>
  <Layout>
    <SEO title="Home" />
    <Home/>
  </Layout>
  </Provider>
)

export default IndexPage
