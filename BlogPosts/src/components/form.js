import React from "react"
import { connect } from "react-redux"
import { addPost, filterByValue } from "./actions"

class AddPost extends React.Component {
  filterByInput = e => {
    let input = e.target.value
    this.props.filterByValue(input.toLowerCase())
  }
  state = {
    id: "",
    title: "",
    body: "",
  }
  handleChange = e => {
    e.preventDefault()
    this.setState({
      [e.target.id]: e.target.value,
    })
  }
  handleSubmit = (e, postsLength) => {
    e.preventDefault()
    this.setState(
      {
        id: (postsLength + 1).toString(),
      },
      () => this.props.addPost(this.state)
    )
  }
  render() {
    let postsLength = this.props.posts.length
    return (
      <div className="container">
        <form
          className="white"
          onSubmit={e => this.handleSubmit(e, postsLength)}
        >
          <div>
            <input
              type="text"
              style={{ height: `50px`, marginTop: 0 }}
              className="inputFields"
              placeholder="Post Title"
              id="title"
              onChange={this.handleChange}
              required
            />
          </div>
          <div>
            <textarea
              id="body"
              placeholder="Post Content"
              className="inputFields"
              style={{ height: 200 }}
              onChange={this.handleChange}
              required
            ></textarea>
          </div>
          <div className="input-field">
            <button className="close">publish</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addPost: post => dispatch(addPost(post)),
    filterByValue: value => dispatch(filterByValue(value)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPost)
