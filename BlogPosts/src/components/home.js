import React from "react"
import { connect } from "react-redux"
import BlogForm from "./form"
import PostsPublished from "./postsPublished"
import { filterByValue } from "./actions"

class Home extends React.Component {
  filterByInput = e => {
    let input = e.target.value
    this.props.filterByValue(input.toLowerCase())
  }
  render() {
    return (
      <div>
        <form
          style={{
            position: `relative`,
            textAlign: "center",
            display: `block`,
            width: `100%`,
          }}
        >
          <div>
            <input
              id="search-input"
              placeholder="Search"
              className="inputFields"
              type="search"
              style={{width: `40%`}}
              onChange={e => {
                this.filterByInput(e)
              }}
            />
            <input type="reset" value="reset" className="close" onClick={e=>{
              this.filterByInput(e)
            }} />
          </div>

          <ul >
            <li >
              Create Post{" "}
            </li>
            <li >
              {" "}
              Published Posts
            </li>
          </ul>
        </form>
        <div
          style={{
            height: `200px`,
            float: `left`,
            position: `relative`,
            width: `49%`,
          }}
        >
          <BlogForm />
        </div>
        <div
          style={{
            overflow: `scroll`,
            height: `700px`,
            float: `right`,
            position: `relative`,
            width: `50%`,
          }}
        >
          <PostsPublished />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    filterByValue: value => dispatch(filterByValue(value)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
