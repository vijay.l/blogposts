import React from 'react'


const validate = values => {
    const errors = {}
    if (!values.title) {
      errors.title = 'Required'
    }
    if (!values.body) {
      errors.body = 'Required'
    }
    return errors
  }


  export const renderField = ({
    input,
    label,
    type,
    style,
    className,
    meta: { touched, error, warning }
  }) => (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={label} type={type} style={style} className={className}/>
        {touched &&
          ((error && <span style={{color: 'red'}}>{error}</span>) ||
            (warning && <span style={{color: "orange"}}>{warning}</span>))}
      </div>
    </div>
  )


  export default validate