export const ADD_POST = 'ADD_POST';
export const FILTER_BY_VALUE = 'FILTER_BY_VALUE'

export const addPost = (post) => {
    return (dispatch, getState) => {
      dispatch({ type: 'ADD_POST', post });
    }
}

export const filterByValue = (value) => {
    return (dispatch, getState) => {
        dispatch({ type: 'FILTER_BY_VALUE', value });
      }
}