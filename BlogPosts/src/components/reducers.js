import { ADD_POST, FILTER_BY_VALUE } from "./actions"

export const initState = {
  appliedFilters: [],
  posts: [
    {
      id: "1",
      title: "Squirtle Laid an Egg",
      body:
        "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur voluptate laborum perferendis, enim repellendus ipsam sunt autem at odit dolorum, voluptatum suscipit iste harum cum magni itaque animi laudantium fugiat",
    },
    {
      id: "2",
      title: "Charmander Laid an Egg",
      body:
        "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur voluptate laborum perferendis, enim repellendus ipsam sunt autem at odit dolorum, voluptatum suscipit iste harum cum magni itaque animi laudantium fugiat",
    },
    {
      id: "3",
      title: "a Helix Fossil was Found",
      body:
        "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur voluptate laborum perferendis, enim repellendus ipsam sunt autem at odit dolorum, voluptatum suscipit iste harum cum magni itaque animi laudantium fugiat",
    },
  ],
}

const rootReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_POST:
      return { ...state, posts: [...state.posts, action.post] }
    case FILTER_BY_VALUE: {
      //clone the state
      let newState = Object.assign({}, state)
      //the value received from our presentational component
      let value = action.value
      let filteredValues = state.posts.filter(post => {
        //look for objects with the received value in their ‘name’ or ‘designer’ fields
        return (
          post.title.toLowerCase().includes(value) ||
          post.body.toLowerCase().includes(value)
        )
      })

      let appliedFilters = state.appliedFilters
      //if the value from the input box is not empty
      if (value && value!=='reset') {
        //check if the filter already exists in the tracking array
        let index = appliedFilters.indexOf(FILTER_BY_VALUE)
        if (index === -1)
          //if it doesn’t, add it.
          appliedFilters.push(FILTER_BY_VALUE)
        //change the filtered products to reflect the change
        newState.filteredPosts = filteredValues
      }
       else {
        //if the value is empty, we can assume everything has been erased
        let index = appliedFilters.indexOf(FILTER_BY_VALUE)
        //in that case, remove the current filter
        appliedFilters.splice(index, 1)
        if (appliedFilters.length === 0 ) {
          //if there are no filters applied, reset the products to normal.
          newState.filteredPosts = newState.posts
        }
      }
      return newState
    }
    default:
      return state
  }
}

export default rootReducer
