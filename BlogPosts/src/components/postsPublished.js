import React from "react"
import { connect } from "react-redux"

var  postsLength
class PostsPublished extends React.Component {
  render() {
    const { posts,filteredPosts, appliedFilters} = this.props
    postsLength=posts.length
    if (!filteredPosts || appliedFilters.length===0) {
      return posts && postsLength &&  posts.map(post => {
        return (
           <div className="posts" key={post.id}>
            <div className="card-content">
              <h3>{post.title}</h3>
              <p>{post.body}</p>
            </div>
          </div>
        )
      })
    } 
    else if ( filteredPosts.length && filteredPosts && appliedFilters.length !==0) {
        return  filteredPosts.map(post => {
          return (
             <div className="posts" key={post.id}>
              <div className="card-content">
                <h3>{post.title}</h3>
                <p>{post.body}</p>
              </div>
            </div>
          )
        })
      }
    else {
      return <div className="center" style={{paddingLeft: 15}}>No posts to show</div>
    }
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts,
    filteredPosts: state.filteredPosts,
    appliedFilters: state.appliedFilters
  }
}

export default connect(mapStateToProps)(PostsPublished)
