
# Welcome to The Blog!

Note:- Hi! This project has been completed as best as I could interpret the problem statement. I have opted not to go with a few things like markdown rendering as that would have meant taking text input, convert and save in markdown then retrieve and recovert markdown to html. I felt it was a bit too unnecesarry and there wasn't much communication to clarify doubts like these. I have used my judgement and completed the project as best as I could. Hope I fit the bill and get recruited.

## Frameworks and libraries used
- Framework used - [GatsbyJS](https://www.gatsbyjs.org/)
- Javascript Library- [ReactJS](https://reactjs.org/)
- Image Processing- [Gatsby Image](https://www.gatsbyjs.org/packages/gatsby-image/)
- Query Language - [Graphql](https://graphql.org/)
- CI/CD and Hosting - [Vercel](https://vercel.com/)

## Steps to deploy website
- Clone [Git Repository](https://gitlab.com/vijay.l/blogposts.git)
- Run Commands from home directory
	1. gatsby build
	2. gatsby serve

## Steps to deploy website
- Clone [Git Repository](https://gitlab.com/vijay.l/blogposts.git)
- Run Commands from home directory
	1. gatsby build
	2. gatsby serve


## Publicly hosted live urls to check project
- [Url 1](https://blogposts.vercel.app/)
- [Url 2](https://blogposts.vijayl.vercel.app)
